package model;

import java.sql.*;
//import java.sql.SQLException;
import org.apache.commons.dbcp2.BasicDataSource;

public class ConnectionPool {
      
    private final String DB="mysql";
    private final String URL="jdbc:mysql://localhost:3306/"+DB+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final String USER="root";
    private final String PASS="123";
    
    private static ConnectionPool dataSource;
    private BasicDataSource basicDataSource=null;
    
    private ConnectionPool(){
     
        basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        basicDataSource.setUsername(USER);
        basicDataSource.setPassword(PASS);
        basicDataSource.setUrl(URL);
        
        basicDataSource.setMinIdle(5);
        basicDataSource.setMaxIdle(20);
        basicDataSource.setMaxTotal(50);
        basicDataSource.setMaxWaitMillis(-1);
        
    }
    
  /*
    private final String DB="mysql";
    private final String URL="jdbc:mysql://localhost:3306/"+DB;
    private final String USER="root";
    private final String PASS="";
    
    
    private static ConnectionPool dataSource;
   // private BasicDataSource basicDataSource=null;
    private Connection con;
    private ConnectionPool() throws ClassNotFoundException, SQLException {
    Class.forName("com.mysql.cj.jdbc.Driver");
    Connection con = DriverManager.getConnection(URL, USER, PASS);
    this.con=con;

     
    /*    basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        basicDataSource.setUsername(USER);
        basicDataSource.setPassword(PASS);
        basicDataSource.setUrl(URL);
        
        basicDataSource.setMinIdle(5);
        basicDataSource.setMaxIdle(20);
        basicDataSource.setMaxTotal(50);
        basicDataSource.setMaxWaitMillis(-1);
        */

    
    public static ConnectionPool getInstance() throws ClassNotFoundException, SQLException {
        if (dataSource == null) {
            dataSource = new ConnectionPool();
            return dataSource;
        } else {
            return dataSource;
        }
    }

    public Connection getConnection() throws SQLException{
     return this.basicDataSource.getConnection();

    }
    
    public void closeConnection(Connection connection) throws SQLException {
        connection.close();
    }    
    
}
